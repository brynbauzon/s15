// ////Assignment Operator

// let x = 2;
// let y = 3;

// console.log(x)
// //expected output: 2

// console.log(x = y + 1); //  3 + 1
// //expected output: 4

// console.log(x = x * y); //  4 * 3
// //expected output: 12



// let bar = 5;

// //Number + Number -> addition
// bar += 2; //7

// //expected output is 7

// // bar -= 2;
// //expected output is 3

// bar *= 2;
// //expected output is 10

// bar /= 2;
// //expected output is 2.5

// console.log(bar);

// ////////Equality Operator

// console.log(1 == 1);
// //expected output is true


// console.log("1" == 1);
// //expected output is true because treat as same value
 

// //Strict Equality Operator

// console.log(1 === 1);
// //expected output is true

// console.log("1" === 1);
// //expected output is false


// //Inequality Operator(!=)

// console.log(1 != 2);
// //expected output is true


// console.log(1 != '1');
// //expected output is false

// console.log (3 !== '3');//true
// console	.log(4 !== 3); //true


// //Relational Operators

// let a = 4;
// let b = 3;

// console.log(a > b);
// // expected output: true

// console.log(a >= b);
// // expected output: true


// console.log(b >= a);
// // expected output: false

// console.log(a < b);
// // expected output: false


// console.log(a <= b);
// // expected output: false

// console.log(a <= 10);
// // expected output: true

// ////////////////Logical AND

// console.log((19 > 11) && (23 < 50));
// // expected output: true

// /////////Logical OR

// console.log((19 > 11) || (23 < 50));
// // expected output: true

// console.log((19 > 11) || (23 === 50));
// // expected output: true

// console.log(!(100 < 150));
// //returns false

//////////////////////Conditional Statements

// let x = 19;

// if ( x === 10){

// 	console.log("equal to 10");
// }else{

// 	console.log("It is not equal 10");

// }


// let x = 23;

// if ( x === 10){

// 	console.log("equal to 10");

// }else if(x > 10){

// 	console.log("x is greater than 10");

// }else{

// 	console.log("x is less than 10");

// }



/*
Create a function called ageChecker that takes in a number as an argument and returns true if the number is greater than or equal to 18 and returns false if otherwise
*/

function ageChecker(number){
	

	if (number >= 18) {
		return true
	} else {
		return false
	}
}

ageChecker();